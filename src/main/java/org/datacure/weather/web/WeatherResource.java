package org.datacure.weather.web;

import org.datacure.weather.service.WeatherService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/weather")
public class WeatherResource {
    @Inject
    WeatherService weatherService;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getWeather(@QueryParam("city") String city) {
        return weatherService.getWeather(city);
    }

    @GET
    @Path("/pdf")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getWeatherPdf(@QueryParam("city") String city) {
        return Response.ok(weatherService.getPdfWeather(city), MediaType.APPLICATION_OCTET_STREAM)
                .header("content-disposition",
                        "attachment; filename = result.pdf").
                build();
    }
}