package org.datacure.weather.web.exception.mappers;

import io.quarkus.arc.Priority;
import org.datacure.weather.owclient.exceptions.OWNotFound;

import javax.ws.rs.Priorities;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.USER)
public class NotFoundExceptionMapper implements ExceptionMapper<OWNotFound> {
    @Override
    public Response toResponse(OWNotFound exception) {
        return Response.status(Response.Status.NOT_FOUND)
                .type(MediaType.TEXT_PLAIN)
                .entity(exception.getMessage())
                .build();
    }
}
