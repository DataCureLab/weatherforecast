package org.datacure.weather.web.exception.mappers;

import io.quarkus.arc.Priority;
import org.datacure.weather.owclient.exceptions.OWException;

import javax.ws.rs.Priorities;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Priority(Priorities.USER + 1)
public class OtherExceptionMapper implements ExceptionMapper<OWException> {
    @Override
    public Response toResponse(OWException exception) {
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                .type(MediaType.TEXT_PLAIN)
                .entity("Internal server error. Try later...")
                .build();
    }
}
