package org.datacure.weather.service.impl;

import org.datacure.weather.mappers.WeatherMapper;
import org.datacure.weather.owclient.ClientType;
import org.datacure.weather.owclient.OpenWeatherClient;
import org.datacure.weather.service.WeatherService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.io.InputStream;

@ApplicationScoped
public class WeatherServiceImpl implements WeatherService {
    @Inject
    @ClientType(ClientType.Digits.CACHED)
    OpenWeatherClient owClient;

    @Inject
    WeatherMapper<String> textMapper;

    @Inject
    WeatherMapper<InputStream> pdfMapper;

    @Override
    public String getWeather(String city) {
        return textMapper.toReport(owClient.getWeather(city));
    }

    @Override
    public InputStream getPdfWeather(String city) {
        return pdfMapper.toReport(owClient.getWeather(city));
    }
}
