package org.datacure.weather.service;

import java.io.InputStream;

public interface WeatherService {
    String getWeather(String city);
    InputStream getPdfWeather(String city);
}
