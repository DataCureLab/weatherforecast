package org.datacure.weather.mappers;

import org.datacure.weather.owclient.client.OWResponse;

public interface WeatherMapper<T> {
    static final String TEMPERATURE = "Temperature";
    static final String HUMID = "Humid";

    T toReport(OWResponse response);
}
