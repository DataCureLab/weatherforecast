package org.datacure.weather.mappers;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.datacure.weather.owclient.client.OWResponse;

import javax.enterprise.context.RequestScoped;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

@RequestScoped
public class PdfWeatherMapper implements WeatherMapper<InputStream> {
    private static final PDFont FONT = PDType1Font.COURIER;
    private static final float FONT_SIZE = 12f;
    private static final float LEADING = 1.5f * FONT_SIZE;

    private static final String TEMP_UNIT = new String(Character.toChars(0x00b0)) + "C";
    private static final String HUMID_UNIT = "%";

    private PDPageContentStream contentStream;
    private PDDocument document;
    private PDPage page;

    @Override
    public InputStream toReport(OWResponse response) {
        try {
            createDocument();
            createPage();
            createParagraph(25f, 700f);
            addText(response.getName());
            addText(String.format(" (%.4f, %.4f)", response.getCoord().getLat(), response.getCoord().getLon()), true);
            addText(String.format("%s: %.1f%s (%.1f%s - %.1f%s)", TEMPERATURE, response.getMain().getTemp(), TEMP_UNIT,
                    response.getMain().getTempMin(), TEMP_UNIT, response.getMain().getTempMax(), TEMP_UNIT), true);
            addText(String.format("%s: %d%s", HUMID, response.getMain().getHumidity(), HUMID_UNIT));
            closeParagraph();
            return prepareDocument();
        } catch (IOException ex) {
            throw new WeatherMapperException("Error while prepare PDF", ex);
        }
    }

    private ByteArrayInputStream prepareDocument() throws IOException {
        ByteArrayOutputStream res = new ByteArrayOutputStream();
        document.save(res);
        document.close();
        return new ByteArrayInputStream(res.toByteArray());
    }

    private void closeParagraph() throws IOException {
        contentStream.endText();
        contentStream.close();
    }

    private void addText(String text) throws IOException {
        addText(text, false);
    }

    private void addText(String text, boolean addNewLine) throws IOException {
        contentStream.showText(text);
        if (addNewLine) {
            contentStream.newLine();
        }
    }

    private void createParagraph(float tx, float ty) throws IOException {
        contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true, true);
        contentStream.beginText();
        contentStream.setFont(FONT, FONT_SIZE);
        contentStream.setLeading(LEADING);
        contentStream.newLineAtOffset(tx, ty);
    }

    private void createPage() {
        page = new PDPage();
        document.addPage(page);
    }

    private void createDocument() {
        document = new PDDocument();
    }
}
