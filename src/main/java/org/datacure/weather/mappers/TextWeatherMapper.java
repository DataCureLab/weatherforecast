package org.datacure.weather.mappers;

import org.datacure.weather.owclient.client.OWResponse;

import javax.enterprise.context.ApplicationScoped;
import java.util.Optional;

@ApplicationScoped
public class TextWeatherMapper implements WeatherMapper<String> {
    private static final String CR = "\r\n";

    @Override
    public String toReport(OWResponse response) {
        StringBuilder res = new StringBuilder();
        res.append(response.getName());
        Optional.ofNullable(response.getCoord()).ifPresent(coord -> res.append(String.format(" (%.4f, %.4f)", coord.getLat(), coord.getLon())));
        res.append(CR);
        res.append(String.format("%s: %.1f (%.1f-%.1f)", TEMPERATURE, response.getMain().getTemp(),
                response.getMain().getTempMin(), response.getMain().getTempMax()));
        res.append(CR);
        res.append(String.format("%s: %d%%", HUMID, response.getMain().getHumidity()));
        return res.toString();
    }
}
