package org.datacure.weather.owclient;

import org.datacure.weather.owclient.client.OWResponse;
import org.datacure.weather.owclient.exceptions.OWException;

public interface OpenWeatherClient {
    OWResponse getWeather(String city) throws OWException;
}
