package org.datacure.weather.owclient;

import org.apache.http.HttpStatus;
import org.datacure.weather.owclient.client.OWResponse;
import org.datacure.weather.owclient.client.OWRestClient;
import org.datacure.weather.owclient.exceptions.OWException;
import org.datacure.weather.owclient.exceptions.OWNotFound;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.WebApplicationException;

@ClientType(ClientType.Digits.STANDARD)
@ApplicationScoped
public class OpenWeatherClientImpl implements OpenWeatherClient {
    private static final String METRIC = "metric";

    @RestClient
    OWRestClient owClient;

    @ConfigProperty(name = "open-weather-appid")
    String appid;

    @Override
    public OWResponse getWeather(String city) {
        try {
            return owClient.getWeather(city, METRIC, appid);
        } catch (WebApplicationException ex) {
            switch (ex.getResponse().getStatus()) {
                case HttpStatus.SC_NOT_FOUND:
                    throw new OWNotFound(city);
                case HttpStatus.SC_UNAUTHORIZED:
                    throw new OWException("Your appid is incorrect", ex);
                default:
                    throw new OWException("Server communication error", ex);
            }

        }
    }
}
