package org.datacure.weather.owclient;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.value.SetArgs;
import io.quarkus.redis.datasource.value.ValueCommands;
import org.datacure.weather.owclient.client.OWResponse;
import org.datacure.weather.owclient.exceptions.OWException;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.Duration;

@ClientType(ClientType.Digits.CACHED)
@ApplicationScoped
public class OpenWeatherClientWithCache implements OpenWeatherClient {
    @Inject
    @ClientType(ClientType.Digits.STANDARD)
    OpenWeatherClient owClient;

    private ValueCommands<String, OWResponse> commands;

    @ConfigProperty(name = "open-weather-cache-ttl")
    Long cacheTimeout;

    public OpenWeatherClientWithCache(RedisDataSource ds) {
        this.commands = ds.value(OWResponse.class);
    }

    @Override
    public OWResponse getWeather(String city) throws OWException {
        var cached = get(city);
        if (cached != null) {
            return cached;
        } else {
            var result = owClient.getWeather(city);
            set(city, result);
            return result;
        }
    }
    public OWResponse get(String key) {
        return commands.get(key);
    }

    public void set(String key, OWResponse result) {
        commands.set(key, result, new SetArgs().ex(Duration.ofSeconds(cacheTimeout)));
    }
}
