package org.datacure.weather.owclient.client;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RegisterRestClient(configKey = "open-weather-api")
@ApplicationScoped
public interface OWRestClient {
    @GET
    @Path("/weather")
    OWResponse getWeather(@QueryParam("q") String city, @QueryParam("units") String units, @QueryParam("appid") String appid);
}
