package org.datacure.weather.owclient.client;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.Instant;
import java.util.List;

@Getter
@Setter
@ToString
public class OWResponse {
    private Long id;
    private String name;
    private Integer cod;
    private Instant timezone;
    private Sys sys;
    private Coord coord;
    private List<Weather> weather;
    private Main main;
    private Wind wind;
    private Clouds clouds;

    @Getter
    @Setter
    @ToString
    public static class Coord {
        private Double lon;
        private Double lat;
    }

    @Getter
    @Setter
    @ToString
    public static class Wind {
        private Double speed;
        private Integer deg;
        private Double gust;
    }

    @Getter
    @Setter
    @ToString
    public static class Weather {
        private Integer id;
        private String main;
        private String description;
        private String icon;
    }

    @Getter
    @Setter
    @ToString
    public static class Main {
        private Double temp;
        @JsonProperty("feels_like")
        private Double feelsLike;
        @JsonProperty("temp_min")
        private Double tempMin;
        @JsonProperty("temp_max")
        private Double tempMax;
        private Integer pressure;
        private Integer humidity;
        @JsonProperty("sea_level")
        private Integer seaLevel;
        @JsonProperty("grnd_level")
        private Integer grndLevel;
    }

    @Getter
    @Setter
    @ToString
    public static class Clouds {
        private Integer all;
    }

    @Getter
    @Setter
    @ToString
    public static class Sys {
        private Integer type;
        private Long id;
        private String country;
        private Instant sunrise;
        private Instant sunset;
    }
}

