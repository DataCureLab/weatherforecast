package org.datacure.weather.owclient.exceptions;

public class OWNotFound extends OWException {
    public OWNotFound(String city) {
        super(String.format("%s not found", city));
    }
}
