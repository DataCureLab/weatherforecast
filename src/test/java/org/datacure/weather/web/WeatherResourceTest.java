package org.datacure.weather.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.datacure.weather.owclient.OWResponseTestData;
import org.datacure.weather.owclient.client.OWResponse;
import org.datacure.weather.owclient.client.OWRestClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@QuarkusTest
class WeatherResourceTest {
    @InjectMock
    @RestClient
    OWRestClient owRestClient;

    @Test
    void testGetWeatherEndpoint() throws JsonProcessingException {
        OWResponse data = OWResponseTestData.getOWResponseData();
        Mockito.when(owRestClient.getWeather(eq(data.getName()), any(), any())).thenReturn(data);

        given()
                .when().get("/weather?city=" + data.getName())
                .then()
                .statusCode(200)
                .body(is(OWResponseTestData.getResultText()));
        Mockito.verify(owRestClient).getWeather(any(), any(), any());
    }

}