package org.datacure.weather.mappers;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.junit.QuarkusTest;
import org.datacure.weather.owclient.OWResponseTestData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

@QuarkusTest
class TextWeatherMapperTest {
    @Inject
    WeatherMapper<String> mapper;

    @Test
    void toReportTest() throws JsonProcessingException {
        Assertions.assertEquals(
                mapper.toReport(OWResponseTestData.getOWResponseData()),
                OWResponseTestData.getResultText());
    }
}
