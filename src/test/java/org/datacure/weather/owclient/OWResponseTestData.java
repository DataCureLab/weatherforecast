package org.datacure.weather.owclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.datacure.weather.owclient.client.OWResponse;

public class OWResponseTestData {
    private static final ObjectMapper mapper = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .registerModule(new JavaTimeModule());

    public static OWResponse getOWResponseData() throws JsonProcessingException {
        String res = "{\n" +
                "  \"coord\": {\n" +
                "    \"lon\": 39.5708,\n" +
                "    \"lat\": 52.6031\n" +
                "  },\n" +
                "  \"weather\": [\n" +
                "    {\n" +
                "      \"id\": 804,\n" +
                "      \"main\": \"Clouds\",\n" +
                "      \"description\": \"overcast clouds\",\n" +
                "      \"icon\": \"04n\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"base\": \"stations\",\n" +
                "  \"main\": {\n" +
                "    \"temp\": 5.68,\n" +
                "    \"feels_like\": 2.71,\n" +
                "    \"temp_min\": 5.68,\n" +
                "    \"temp_max\": 5.68,\n" +
                "    \"pressure\": 1020,\n" +
                "    \"humidity\": 81,\n" +
                "    \"sea_level\": 1020,\n" +
                "    \"grnd_level\": 1001\n" +
                "  },\n" +
                "  \"visibility\": 10000,\n" +
                "  \"wind\": {\n" +
                "    \"speed\": 3.97,\n" +
                "    \"deg\": 333,\n" +
                "    \"gust\": 9.03\n" +
                "  },\n" +
                "  \"clouds\": {\n" +
                "    \"all\": 98\n" +
                "  },\n" +
                "  \"dt\": 1682977557,\n" +
                "  \"sys\": {\n" +
                "    \"type\": 2,\n" +
                "    \"id\": 2080875,\n" +
                "    \"country\": \"RU\",\n" +
                "    \"sunrise\": 1682992139,\n" +
                "    \"sunset\": 1683046102\n" +
                "  },\n" +
                "  \"timezone\": 10800,\n" +
                "  \"id\": 535121,\n" +
                "  \"name\": \"Lipetsk\",\n" +
                "  \"cod\": 200\n" +
                "}";
        return mapper.readValue(res, OWResponse.class);
    }

    public static String getResultText() {
        return "Lipetsk (52,6031, 39,5708)\r\n" +
                "Temperature: 5,7 (5,7-5,7)\r\n" +
                "Humid: 81%";
    }
}
