package org.datacure.weather.owclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectMock;
import org.datacure.weather.owclient.client.OWResponse;
import org.datacure.weather.owclient.client.OWRestClient;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@QuarkusTest
class OpenWeatherClientTest {
    @InjectMock
    @RestClient
    OWRestClient owRestClient;

    @Inject
    @ClientType(ClientType.Digits.STANDARD)
    OpenWeatherClient client;

    @Test
    void testOpenWeatherClient() throws JsonProcessingException {
        OWResponse data = OWResponseTestData.getOWResponseData();
        Mockito.when(owRestClient.getWeather(eq(data.getName()), any(), any())).thenReturn(data);
        Assertions.assertEquals(client.getWeather(data.getName()), data);
        Mockito.verify(owRestClient).getWeather(any(), any(), any());
    }
}
